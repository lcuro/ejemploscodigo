
<?php


class Categoria extends Eloquent {

	protected $table="categorias";

	protected $primaryKey="id";


	public function productos()
	{
		return $this->hasMany("Producto","idcategoria");
	} 

}
